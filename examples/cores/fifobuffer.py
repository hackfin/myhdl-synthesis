# FIFO buffer implementation
#
# (c) 2020 section5.ch
#
#
from myhdl import *
from synthesis.yosys.autowrap import autowrap, BulkSignal

class MemPort:
	def __init__(self, DATA_W = 16, ADDR_W = 10):
		self.we = Signal(bool())
		self.addr = Signal(modbv()[ADDR_W:])
		self.write = Signal(modbv()[DATA_W:])
		self.read = Signal(modbv()[DATA_W:])

class MemWritePort(BulkSignal):
	__slots__ = [
		'we',
		'addr',
		'write',
	]

	def __init__(self, name, DATA_W = 16, ADDR_W = 10):
		self.we = Signal(bool())
		self.addr = Signal(modbv()[ADDR_W:])
		self.write = Signal(modbv()[DATA_W:])
		super().__init__(name, False)


class MemReadPort(BulkSignal):
	__slots__ = [
		'read',
	]
	def __init__(self, name, DATA_W = 16):
		self.read = Signal(modbv()[DATA_W:])
		super().__init__(name, True)

@autowrap
def bram_2psync(clk, a_in, b_in, a_out, b_out, ADDR = 6, DATA = 16):
	"Auto-wrapped bram.v black box"
	mem = [ modbv()[DATA:] for i in range(2 ** ADDR) ]
	addr_a, addr_b = [ Signal(modbv()[ADDR:]) for i in range(2) ]
	
	@always(clk.posedge)
	def assign():
		addr_a.next = a_in.addr
		addr_b.next = b_in.addr

	@always_comb
	def mem_read():
		a_out.read.next = mem[addr_a]
		b_out.read.next = mem[addr_b]

	@always(clk.posedge)
	def mem_write():
		mem[b_in.addr] = b_in.write
		
	return instances()


@autowrap
def _fifobuffer(wren, idata, iready, odata, oready, rden, err, reset, clk, \
	ADDR_W = 6, DATA_W = 16, EXTRA_REGISTER = False, SYN_RAMTYPE = 'block_ram'):

	wren.read = True
	
	@always(clk.posedge)
	def assign():
		iready.next = False
		odata.next = 0xff
		oready.next = False
		err.next = False
		
	return instances()

@block
def fifobuffer(wren, idata, iready, odata, oready, rden, err, reset, clk, \
	ADDR_W = 6, DATA_W = 8, EXTRA_REGISTER = False, SYN_RAMTYPE = 'block_ram'):

	state_t = enum('S_IDLE', 'S_READY', 'S_FULL', 'S_ERROR')
	state = Signal(state_t.S_IDLE)
	# Wraparound pointer:
	iptr, optr = [ Signal(modbv(0)[ADDR_W:]) for _ in range(2) ]
	next_iptr, next_optr = [ Signal(modbv(0)[ADDR_W:]) for _ in range(2) ]
	dready = Signal(bool())
	rdata = Signal(intbv()[DATA_W:])

	names = [ 'a', 'b' ]
	ai, bi = [ MemWritePort(names[i], DATA_W=DATA_W, ADDR_W=ADDR_W) \
		for i in range(2) ]
	ao, bo = [ MemReadPort(names[i], DATA_W=DATA_W) \
		for i in range(2) ]

	int_full, int_rden = [ Signal(bool()) for _ in range(2) ]
	if EXTRA_REGISTER:
		int_rden_d = Signal(bool())

	maybe_full, maybe_empty = [ Signal(bool(0)) for _ in range(2) ]

	inst_bram = bram_2psync(clk, ai, bi, ao, bo, ADDR=ADDR_W, DATA=DATA_W)
	
	@always_seq(clk.posedge, reset)
	def count():
		if wren:
			iptr.next = next_iptr
		if int_rden:
			optr.next = next_optr

	@always_comb
	def logic():
		rdata.next = ao.read
		bi.we.next = wren
		bi.addr.next = iptr
		bi.write.next = idata
		ai.addr.next = optr
		ai.we.next = False
		ai.write.next = 0

		next_iptr.next = iptr + 1
		next_optr.next = optr + 1

		err.next = state == state_t.S_ERROR
		iready.next = not int_full

	@always_comb
	def fill_state():
		maybe_full.next = optr == next_iptr
		maybe_empty.next = iptr == next_optr
		dready.next = state == state_t.S_READY or state == state_t.S_FULL

	@always_seq(clk.posedge, reset)
	def fsm():
		if state == state_t.S_IDLE:
			if wren:
				state.next = state_t.S_READY
		elif state == state_t.S_READY:
			if wren:
				if maybe_full and not int_rden:
					state.next = state_t.S_FULL
			elif maybe_empty and int_rden:
				state.next = state_t.S_IDLE
		elif state == state_t.S_FULL:
			if wren:
				if not int_rden:
					state.next = state_t.S_ERROR
			elif int_rden:
				state.next = state_t.S_READY
			else:
				state.next = state_t.S_FULL
		elif state == state_t.S_ERROR:
			pass


	if EXTRA_REGISTER:
		@always_comb
		def handle_read_register():
			int_rden.next = (not int_full or rden) and dready


		@always_seq(clk.posedge, reset)
		def preread():
			
			if dready:
				if int_full == False:
					int_full.next = True
					oready.next = True
			elif int_full:
				if rden:
					oready.next = False
					int_full.next = False
				else:
					oready.next = True
			else:
				oready.next = False

			int_rden_d.next = int_rden
			if int_rden_d:
				odata.next = rdata
	else:
		@always_comb
		def handle_read():
			int_full.next = state == state_t.S_FULL
			int_rden.next = rden
			odata.next = rdata
			oready.next = dready
	

	return instances()
