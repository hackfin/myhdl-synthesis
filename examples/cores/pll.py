from myhdl import *
from synthesis.yosys.ecp5.components import EHXPLLL

@block
def pll2(clki, clk0, clk1, lock, PLL_CONFIG, INT_FEEDBACK = False ):
	intlock, lock0 = [ Signal(bool()) for i in range(2) ]
	clkop, clkos, clkos2, clkos3 = [ Signal(bool()) for i in range(4) ]
	refclk, clkintfb = [ Signal(bool()) for i in range(2) ]
	fb, intlock = [ Signal(bool()) for i in range(2) ]

	e = EHXPLLL(clki, fb, False, False, False, False, \
		False, False, \
		False, False, \
		False, False, False, False, \
		clkop, clkos, clkos2, clkos3, lock0, intlock, \
		refclk, clkintfb, \
		**PLL_CONFIG)

	@always_comb
	def assign():
		clk0.next = clkop
		clk1.next = clkos

		if INT_FEEDBACK:
			fb.next = clkintfb
		else:
			fb.next = clkop

		lock.next = lock0
	
	return instances()
