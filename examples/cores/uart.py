# UART example
#
# (c) 2010-2020, <hackfin@section5.ch>
#
#

from myhdl import *
from myhdl.conversion import yshelper
from synthesis.yosys.autowrap import BulkSignal

try:
	from .fifobuffer import fifobuffer
except:
	from fifobuffer import fifobuffer


HAVE_FIFO = True

uart_state_t = enum('S_IDLE', 'S_START', 'S_SHIFT', 'S_STOP', 'S_ERR')

@block
def counter(clk, en, rst, count):

	@always(clk.posedge)
	def worker():
		if rst:
			count.next = 0
		elif en:
			count.next = count + 1

	return instances()

@block
def uart_tx(busy, data, data_ready, advance, tx, reset, txclken, clk):

	state = Signal(uart_state_t.S_IDLE)
	dsr = Signal(intbv(0)[8:])
	bitcount = Signal(modbv()[3:])
	brst = Signal(bool(0))
	lastbit = Signal(bool(0))

	@always(clk.posedge)
	def fsm():
		advance.next = False
		if reset:
			state.next = uart_state_t.S_IDLE
		elif txclken:
			if state == uart_state_t.S_IDLE or state == uart_state_t.S_STOP:
				if data_ready:
					state.next = uart_state_t.S_START;
				else:
					state.next = uart_state_t.S_IDLE;
			elif state == uart_state_t.S_START:
				state.next = uart_state_t.S_SHIFT;
				dsr.next = data
				advance.next = True
			elif state == uart_state_t.S_SHIFT:
				dsr.next = concat(True, dsr[8:1])
				if lastbit:
					state.next = uart_state_t.S_STOP
			else:
				state.next = uart_state_t.S_IDLE

	bc_inst = counter(clk, txclken, brst, bitcount)

	@always(clk.posedge)
	def shift():
		lastbit.next = bitcount == 7

	@always_comb
	def assign0():
		busy.next = state != uart_state_t.S_IDLE
		is_shift = state == uart_state_t.S_SHIFT
		brst.next = not is_shift

		if state == uart_state_t.S_START:
			tx.next = False
		elif state == uart_state_t.S_SHIFT:
			tx.next = dsr[0]
		else:
			tx.next = True

	return instances()

@block
def uart_rx(debug, rx, err_frame, data, strobe, reset, clk16en, clk,
	CLKDIV2 = 3):
	"UART receiver unit"

	state = Signal(uart_state_t.S_IDLE)
	rx_d = Signal(bool())
	bcen, brst = [ Signal(bool(0)) for i in range(2) ]
	start_err, frame_err = [ Signal(bool(0)) for i in range(2) ]
	rxtrigger = Signal(bool(0))
	strobeq, strobe_i = [ Signal(bool(0)) for i in range(2) ]
	bitcount = Signal(modbv()[3:])
	count = Signal(modbv(0)[CLKDIV2+1:])

	is_count_begin, is_count_mid = [ Signal(bool(0)) for i in range(2) ]
	dsr = Signal(intbv(0)[8:])

  
	@always_seq(clk.posedge, reset)
	def generate_rxclk_en():
		if clk16en:
			rx_d.next = rx
			if state == uart_state_t.S_IDLE:
				count.next = 0
			else:
				count.next = count + 1
 
	@always(count)
	def mid_sample():
		is_count_begin.next = count == 15
		is_count_mid.next = count == 7
		
	@always_seq(clk.posedge, reset)
	def fsm():
		if clk16en:
			if state == uart_state_t.S_IDLE:
				if rxtrigger:
					state.next = uart_state_t.S_START
				elif rx == False:
					state.next = uart_state_t.S_ERR
				else:
					state.next = uart_state_t.S_IDLE
			elif state == uart_state_t.S_START:
				if is_count_begin:
					state.next = uart_state_t.S_SHIFT
			elif state == uart_state_t.S_SHIFT:
				if is_count_begin and bitcount == 7:
					state.next = uart_state_t.S_STOP
			else:
				pass

		elif state == uart_state_t.S_STOP:
			state.next = uart_state_t.S_IDLE
				
 
	@always(clk.posedge)
	def shift():
		if clk16en and is_count_mid:
			if state == uart_state_t.S_SHIFT:
				dsr.next = concat(rx, dsr[8:1])
  
	@always_comb
	def assign0():
		rxtrigger.next = rx_d and not rx
		strobe_i.next = state == uart_state_t.S_SHIFT
   
	@always(clk.posedge)
	def tx_strobe():
		strobeq.next = strobe_i
		data.next = dsr
   
	@always_comb
	def assign1():
		strobe.next = strobeq and not strobe_i
		err_frame.next = frame_err or start_err
		debug.next = bitcount
		is_shift = state == uart_state_t.S_SHIFT
		brst.next = not is_shift
		bcen.next = clk16en and is_count_begin

	bc_inst = counter(clk, bcen, brst, bitcount)
   
	@always_seq(clk.posedge, reset)
	def detect_frerr():
		if state == uart_state_t.S_STOP and \
			is_count_mid == True and rx == False:
			frame_err.next = True

	@always_seq(clk.posedge, reset)
	def detect_start_err():
		if state == uart_state_t.S_ERR:
			start_err.next = True
 
	return instances()

class uart_WritePort(BulkSignal):
	__slots__ = [
		'uart_clkdiv',
		'rx_irq_enable',
		'uart_reset',
		'uart_txr',
		'select_uart_rxr',
		'select_uart_txr'
	]

	def __init__(self, name):
		self.uart_clkdiv = Signal(modbv()[10:])
		self.rx_irq_enable = Signal(bool())
		self.uart_reset = ResetSignal(0, 1, False)
		self.uart_txr = Signal(modbv()[8:])
		self.select_uart_rxr, self.select_uart_txr = \
			[ Signal(bool()) for i in range(2) ]
		super().__init__(name, False)

class uart_ReadPort(BulkSignal):
	__slots__ = [
		'rxready',
		'txready',
		'txbusy',
		'frerr',
		'txovr',
		'rxovr',
		'bitcount',
		'rxdata',
		'dvalid'
	]

	def __init__(self, name):
		self.rxready = Signal(bool())
		self.txready = Signal(bool())
		self.txbusy = Signal(bool())
		self.frerr = Signal(bool())
		self.txovr = Signal(bool())
		self.rxovr = Signal(bool())
		self.bitcount = Signal(modbv()[3:])
		self.rxdata = Signal(modbv()[8:])
		self.dvalid = Signal(bool())
		super().__init__(name, True)

@block
def clock_div(clk, rst, ce, txen, clkdiv):
	counter = Signal(intbv()[16:])
	count16 = Signal(modbv()[4:])

	@always(clk.posedge)
	def worker():
		ce.next = False
		txen.next = False
		if rst:
			counter.next = 0
			count16.next = 0
		elif counter == clkdiv:
			counter.next = 0
			ce.next = True
			count16.next = count16 + 1
			if count16 == 15:
				txen.next = True
		else:
			counter.next = counter + 1

	return instances()

@block
def uart_core(tx, rx, rxirq, ctrl, stat, clk):
	"UART top level peripheral controller"
	txd, rxd = [ Signal(intbv()[8:]) for i in range(2) ]
	strobe_rx = Signal(bool())
	iready = Signal(bool())
	txfifo_dready, txfifo_strobe = [ Signal(bool()) for i in range(2) ]
	bitcount = Signal(intbv()[3:])

	rxfifo_data, txfifo_data  = [ Signal(intbv()[8:]) for i in range(2) ]

	rxfifo_rden, txfifo_wren, rxdata_ready, txfifo_dready, txfifo_strobe = \
		[Signal(bool()) for i in range(5)]
	
	clk16_enable, txclk_enable = [ Signal(bool()) for i in range(2) ]
	
	clkdiv_inst = clock_div(clk, ctrl.uart_reset, clk16_enable, \
		txclk_enable, ctrl.uart_clkdiv)
	
	rx_inst = uart_rx(bitcount, rx, stat.frerr, rxd, strobe_rx, \
		ctrl.uart_reset, clk16_enable, clk)
	tx_inst = uart_tx(stat.txbusy, txd, txfifo_dready, txfifo_strobe, tx, \
		ctrl.uart_reset, txclk_enable, clk)
	
	rxfifo_inst = fifobuffer(strobe_rx, rxd, iready, rxfifo_data,
		rxdata_ready, rxfifo_rden, stat.rxovr, ctrl.uart_reset, clk,
		ADDR_W = 6 )

	txfifo_inst = fifobuffer(txfifo_wren, txfifo_data, \
		stat.txready, txd, txfifo_dready, txfifo_strobe, stat.txovr, \
		ctrl.uart_reset, clk, ADDR_W = 10 )

	@always_comb
	def assign():
		rxfifo_rden.next = ctrl.select_uart_rxr and rxdata_ready
		txfifo_wren.next = ctrl.select_uart_txr
		txfifo_data.next = ctrl.uart_txr
		stat.rxdata.next = rxfifo_data
		stat.rxready.next = rxdata_ready
		stat.bitcount.next = bitcount
		stat.dvalid.next = rxdata_ready
		rxirq.next = rxdata_ready and ctrl.rx_irq_enable

	if HAVE_FIFO:
		pass
	else:
		@always_comb
		def wire():
			txd.next = ctrl.uart_txr
			stat.rxdata.next = rxd
	return instances()

@block
def test_unit(ctrl, stat):
	txd, rxd = [ Signal(intbv()[8:]) for i in range(2) ]
	clk = Signal(bool(0))
	strobe_rx = Signal(bool())
	txfifo_dready, txfifo_strobe = [ Signal(bool()) for i in range(2) ]

	clk16_enable, txclk_enable = [ Signal(bool()) for i in range(2) ]
	
	clkdiv_inst = clock_div(clk, ctrl.uart_reset, clk16_enable, \
		txclk_enable, ctrl.uart_clkdiv)
	
	rx_inst = uart_rx(stat.bitcount, rx, stat.frerr, rxd, strobe_rx, \
		ctrl.uart_reset, clk16_enable, clk)
	tx_inst = uart_tx(stat.txbusy, txd, txfifo_dready, txfifo_strobe, tx, \
		ctrl.uart_reset, txclk_enable, clk)

	@always_comb
	def loopback():
		rx.next = tx

	@always(delay(1))
	def clkgen():
		clk.next  = not clk

	@always(clk.posedge)
	def check_data():
		if strobe_rx:
			print("Got data %02x" % rxd)

	data = [ 0x95, 0x21, 0xcd, 0x00, 0x55 ]

	@instance
	def stimulate():
		ctrl.uart_reset.next = True
		ctrl.uart_clkdiv.next = 4
		txfifo_strobe.next = False
		for i in range(4):
			yield clk.posedge
		ctrl.uart_reset.next = False
		yield delay(40)

		for d in data:
			if stat.txbusy:
				yield clk.posedge
			txd.next = d
			yield clk.posedge
			txfifo_dready.next = True
			yield txfifo_strobe.posedge
			txfifo_dready.next = False

			yield strobe_rx.negedge
			if rxd != d:
				for i in range(16 * 10):
					yield clk.posedge
				raise ValueError("Invalid value received, expected %02x, got %02x" % (d, rxd))

		raise StopSimulation
	
	return instances()

if __name__ == '__main__':
	clk = Signal(bool())
	rxirq = Signal(bool())

	# Classes still must have the same name
	uart_ctrl = uart_WritePort("ctrl")
	uart_stat = uart_ReadPort("stat")
	tx, rx = [ Signal(bool()) for i in range(2) ]

	u = uart_core(tx, rx, rxirq, uart_ctrl, uart_stat, clk)
	design = yshelper.Design("uart")
	u.convert("yosys_module", design)

	design.display_rtl("uart_rx_3_1_1_8_1_1_1_1")
	# design.display_rtl("bram_2psync_1")
	# design.display_rtl("", fmt="ps")
	
	tb = test_unit(uart_ctrl, uart_stat)
	tb.config_sim(backend = 'myhdl', timescale="1ps", trace=True)
	tb.run_sim(20000)
	tb.quit_sim() # Quit so we can run another one

