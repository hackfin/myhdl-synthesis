# ECP5 auxiliaries
#
#

from synthesis.yosys.ecp5 import rules

def synthesize(design):

	r = rules.SynthesisRulesECP5()
	r.map(design)


_t = rules.TECHLIBS_PATH + '/ecp5/'
simulation_files = [ _t + "cells_sim.v", _t + "cells_bb.v" ]
