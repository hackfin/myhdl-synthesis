# Generator for PLL values
#
# 2016, <hackfin@section5.ch>
#
# 2020: Version for ECP5 EHXPLLL, taken from ehxpll tool
#
#

import sys

REF_DIV_RANGE = (1, 128)
FBMUL_RANGE = (1, 80)
SEC_DIV_RANGE = (1, 128)

VCO_RANGE = (400.0, 800.0)
PFD_RANGE = (3.125, 400.0)
INPUT_RANGE = (8.0, 400.0)
OUTPUT_RANGE = (10.0, 400.0)

FLOAT_MAX = sys.float_info.max

# For PLL generator:
from myhdl import *
from synthesis.yosys.ecp5.components import EHXPLLL

def translate(x):
	if isinstance(x, bool):
		if x:
			return "ENABLED"
		else:
			return "DISABLED"
	return x

def ratprox(r, P, Q):
	"""Rational approximator, cheap and dirty"""
	inverse = False
	if r > 1.0:
		r = 1.0
		inverse = True
	a = (0, 1)
	b = (1, 1)

	c = (a[0] + b[0], a[1] + b[1])
	s = r * b[1]
	da, db = s, b[0] - s
	if da > db:
		p = b
	else:
		p = a

	while c[0] <= P and c[1] <= Q:
		sc = r * c[1]
		sp = r * p[1]

		da, db = abs(c[0] - sc), abs(p[0] - sp)
		if da < db:
			p = c

		if r * a[1] >= a[0] and sc <= c[0]:
			b = c
		else:
			a = c

		c = (a[0] + b[0], a[1] + b[1])
	if inverse:
		return (p[1], p[0])
	return p

def inrange(x, y):
	return x >= y[0] and x <= y[1]

class secondary_params:
	def __init__(self, i):
		self.enabled = False
		self.div = 0
		self.cphase = 0
		self.fphase = 0
		self.name = "clkout[%d]" % i
		self.freq = 0.0
		self.phase = 0.0

SIMPLE, HIGHRES = range(2)

import math

def divisors(n):
	s = int(math.sqrt(n)) + 1
	d = [ i for i in range(2, s) if n % i == 0 ]
	return d

def calc_pll(f_in, f_out):
	Q = P = 64	  

	s = f_out / f_in

	fbmul, idiv = ratprox(s, P, Q)
	f_pfd = f_in / idiv
	divs = divisors(idiv)

	print("mul: %d, div: %d" % (fbmul, idiv))

	k = 1

	# If we don't reach minimum required PDF,
	# retry with more granular P/Q range
	if f_pfd < PFD_RANGE[0]:
		print("PFD range not met: %f" % (f_pfd))
		max_idiv = int(f_in / PFD_RANGE[0])
		if Q > max_idiv:
			Q = max_idiv
		if P > Q:
			P = Q
		# Redo the thing
		fbmul, idiv = ratprox(s, P, Q)
		f_pfd = f_in / idiv
		divs = divisors(idiv)		 
		print("new mul: %d, div: %d" % (fbmul, idiv))
		
	if fbmul < 1:
		raise ValueError("Can't compensate")
		
	if not inrange(f_pfd, PFD_RANGE):
		raise ValueError("PFD out of range")
			
	f_vco = f_pfd * fbmul

	if f_vco < VCO_RANGE[0]:
		print("too low VCO", f_vco)
		mk = VCO_RANGE[0] / f_vco
		s = 1.0 / (mk + 0.1)

		p1 = ratprox(s, 1, 128)

		if p1[0] == 0:
			raise ValueError("Unable to compensate low VCO")
		f_vco *= p1[1]	  

		k *= p1[1]
			  
	it = iter(divs)

	try:
		while f_vco > VCO_RANGE[1]:
			d = next(it)
			while d and idiv % d == 0 and f_vco > VCO_RANGE[1]:
				k //= d
				f_vco /= d
				print("new IDIV", idiv, "new VCO", f_vco)
				fbmul *= d

	except StopIteration:
		raise ValueError("Unable to reach lower VCO value")

	f_out = f_vco / k
	
	if not inrange(fbmul, FBMUL_RANGE):
		raise ValueError("Feedback multiplicator out of valid range")
	
	return (k, fbmul, idiv, f_out, f_vco)

class EHXPLLConfig:
	VCO_RANGE = (400.0, 800.0)
	PFD_RANGE = (3.125, 400.0)
	INPUT_RANGE = (8.0, 400.0)
	OUTPUT_RANGE = (10.0, 400.0)


	OPTIONS_DEFAULT = {
		"highres" : False,
		"reset" : False,
		"standby" : False,
		"clkout1" : None,
		"clkout2" : None,
		"clkout3" : None,
	}

#	class options:
#		__slots__ = [ 
#			"clkin_name",
#			"clkout0_name",
#			"clkout1_name",
#			"clkout1",
#			"phase1",
#			"clkout2_name",
#			"clkout2",
#			"phase2",
#			"clkout3_name",
#			"clkout3",
#			"phase3",
#			"file",
#			"highres",
#			"dynamic",
#			"reset",
#			"standby",
#			"feedback_clkout",
#			"internal_feedback",
#			"internal_feedback_wake"
#		]


	class pll_params:
		FB_NAMES = [ "OP", "OS", "OS2", "OS3" ]

		def __init__(self):
			self.mode = SIMPLE
			self.refclk_div = 0
			self.feedback_mul = 0
			self.output_div = 1;
			self.primary_cphase = 0;
			self.clkin_name = ""
			self.clkout0_name = ""
			self.dynamic = False
			self.reset = False
			self.standby = False
			self.feedback_clkout = 0
			self.internal_feedback = False # Creates wiring (clkfb) when True
			self.internal_feedback_wake = False
			self.feedback_name = self.FB_NAMES
			self.feedback_wname = [ "" for i in range(4) ]

			self.clkin_frequency = 8.0

			self.secondary = [ secondary_params(i) for i in range(3) ]




	class parameters:
		__slots__ = [
			"PLLRST_ENA",
			"INTFB_WAKE",
			"STDBY_ENABLE",
			"DPHASE_SOURCE",
			"OUTDIVIDER_MUXA",
			"OUTDIVIDER_MUXB",
			"OUTDIVIDER_MUXC",
			"OUTDIVIDER_MUXD",
			"CLKI_DIV",
			"CLKOP_ENABLE",
			"CLKOP_DIV",
			"CLKOP_CPHASE",
			"CLKOP_FPHASE",
			"CLKOP_TRIM_DELAY",
			"CLKOP_TRIM_POL",
			"CLKOS_ENABLE",
			"CLKOS_DIV",
			"CLKOS_CPHASE",
			"CLKOS_FPHASE",
			"CLKOS_TRIM_DELAY",
			"CLKOS_TRIM_POL",
			"CLKOS2_ENABLE",
			"CLKOS2_DIV",
			"CLKOS2_CPHASE",
			"CLKOS2_FPHASE",
			"CLKOS3_ENABLE",
			"CLKOS3_DIV",
			"CLKOS3_CPHASE",
			"CLKOS3_FPHASE",
			"FEEDBK_PATH",
			"CLKFB_DIV"
		]

		def __init__(self):
			for i in self.__slots__:
				setattr(self, i, None)


		def dictionary(self):
			d = { n : translate(getattr(self, n)) for n in self.__slots__ \
				if not (n.startswith("_")) }
			return d

	def __init__(self, f_in, **opts):
		self.ehxpll_parameters = self.parameters()
		self.opt = self.OPTIONS_DEFAULT
		self.opt.update(opts)
		self.params = self.pll_params()
		self.f_in = f_in

		p = self.ehxpll_parameters
		p.PLLRST_ENA = False
		p.INTFB_WAKE = False
		p.STDBY_ENABLE = False
		p.DPHASE_SOURCE = False
		p.OUTDIVIDER_MUXA = "DIVA"
		p.OUTDIVIDER_MUXB = "DIVB"
		p.OUTDIVIDER_MUXC = "DIVC"
		p.OUTDIVIDER_MUXD = "DIVD"
#		p.CLKI_DIV = 4
#		p.CLKOP_ENABLE = True
#		p.CLKOP_DIV = 6
#		p.CLKOP_CPHASE = 5
#		p.CLKOP_FPHASE = 0
#		p.CLKOP_TRIM_DELAY = 0
#		p.CLKOP_TRIM_POL = "FALLING"
#		p.CLKOS_ENABLE = True
#		p.CLKOS_DIV = 30
#		p.CLKOS_CPHASE = 29
#		p.CLKOS_FPHASE = 0
#		p.CLKOS_TRIM_DELAY = 0
#		p.CLKOS_TRIM_POL = "FALLING"
#		p.CLKOS2_ENABLE = True
#		p.CLKOS2_DIV = 15
#		p.CLKOS2_CPHASE = 14
#		p.CLKOS2_FPHASE = 0
#		p.CLKOS3_ENABLE = True
#		p.CLKOS3_DIV = 10
#		p.CLKOS3_CPHASE = 9
#		p.CLKOS3_FPHASE = 0
#		p.FEEDBK_PATH = "CLKOP"
#		p.CLKFB_DIV = 5

		self.f_out = 0.0
		self.f_vco = 0.0

	def calc_derived(self, which, name, frequency, phase):
		"Calculate derived frequency dividers"
		c = self.params
		div = self.f_vco / frequency

		sc = c.secondary[which]

		sc.enabled = True
		sc.div = int(div)
		sc.freq = self.f_vco / div
		if phase == 1 or phase == 1.0:
			sc.cphase = int(div) - 1
		else:
			sc.cphase = self.primary_cphase + int(phase * div)
		sc.fphase = 0
		sc.name = name

	def calc_pll(self, f_out, phase):
		print("Calculate PLL values for %.2f MHz" % f_out)
		k, fbmul, idiv, f_eff_out, f_vco = calc_pll(self.f_in, f_out)
		p = self.params	
		p.refclk_div = idiv
		p.feedback_mul = fbmul
		p.output_div = k
		self.f_out = f_eff_out
		self.f_vco = f_vco
		# 180 degree phase:
		p.primary_cphase = int(phase * k)

	def setClk(self, f_out, phase = 0.5, which = 0):
		if not inrange(self.f_in, self.INPUT_RANGE):
			raise ValueError("Bad input frequency (must be in %s)" % \
				self.INPUT_RANGE)
		if not inrange(f_out, self.OUTPUT_RANGE):
			raise ValueError("Bad output frequency (must be in %s)" % \
				self.OUTPUT_RANGE)

		p = self.params
		p.clkin_frequency = self.f_in
		p.clkin_name = "clkin"

		arg = self.opt

		if which == 0:
			self.calc_pll(f_out, phase)
		else:
			n = "clkout%d" % which
			self.calc_derived(which - 1, n, f_out, phase)



	def configure(self):
		e = self.ehxpll_parameters
		p = self.params

		e.PLLRST_ENA = p.reset
		e.INTFB_WAKE = p.internal_feedback_wake
		e.CLKFB_DIV = p.feedback_mul

		e.STDBY_ENABLE = p.standby
		e.DPHASE_SOURCE = p.dynamic
		e.OUTDIVIDER_MUXA = "DIVA"
		e.OUTDIVIDER_MUXB = "DIVB"
		e.OUTDIVIDER_MUXC = "DIVC"
		e.OUTDIVIDER_MUXD = "DIVD"
		e.CLKI_DIV = p.refclk_div
		e.CLKOP_ENABLE = True
		e.CLKOP_DIV = p.output_div
		e.CLKOP_CPHASE = p.primary_cphase
		e.CLKOP_FPHASE = 0
		e.CLKOP_TRIM_DELAY = 0
		e.CLKOP_TRIM_POL = "FALLING"

		if p.internal_feedback:
			pre = "INT_"
		else:
			pre = "CLK"

		e.FEEDBK_PATH = pre + p.feedback_name[p.feedback_clkout]

		for i in range(3):
			sc = p.secondary[i]
			n = p.feedback_name[i + 1]
			pre = "CLK" + n
			setattr(e, pre + "_ENABLE", sc.enabled)
			if sc.enabled:
				setattr(e, pre + "_DIV", sc.div)
				setattr(e, pre + "_CPHASE", sc.cphase)
				setattr(e, pre + "_FPHASE", sc.fphase)
				setattr(e, pre + "_TRIM_DELAY", 0)
				setattr(e, pre + "_TRIM_POL", "FALLING")

		return e.dictionary()

	@block
	def generate(self, clki, lock, clk0, **signals):
		cfg = self.configure()

		intlock, lock0 = [ Signal(bool()) for i in range(2) ]
		clkfb, clkop, clkos, clkos2, clkos3 = \
			[ Signal(bool()) for i in range(5) ]
		refclk, clkintfb = [ Signal(bool()) for i in range(2) ]
		fb, intlock = [ Signal(bool()) for i in range(2) ]

		e = EHXPLLL(clki, fb, False, False, False, False, \
			False, False, \
			False, False, \
			False, False, False, False, \
			clkop, clkos, clkos2, clkos3, lock0, intlock, \
			refclk, clkintfb, \
			**cfg)

		if 'clk1' in signals:
			clk1 = signals['clk1']
			print("Enable CLK1")
			@always_comb
			def assign_clk1():
				clk1.next = clkos
			
		if 'clk2' in signals:
			clk2 = signals['clk2']
			print("Enable CLK2")
			@always_comb
			def assign_clk2():
				clk2.next = clkos2
			
		if 'clk3' in signals:
			print("Enable CLK3")
			clk3 = signals['clk2']
			@always_comb
			def assign_clk3():
				clk3.next = clkos3
			
		@always_comb
		def assign():
			clk0.next = clkop

			if self.params.internal_feedback:
				fb.next = clkintfb
			else:
				fb.next = clkop

			lock.next = lock0
		
		return instances()	

