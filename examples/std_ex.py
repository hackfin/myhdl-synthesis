from myhdl.conversion import yshelper as ys

from myhdl import *

def standard_conversion(UNIT, arst):

	# Declare the signals:	  
	clk = Signal(bool())
	ce = Signal(bool())
	dout, debug = [ Signal(intbv()[8:]) for i in range(2) ]
	reset = ResetSignal(0, 1, isasync = arst)

	def convert(unit, clk, ce, reset, dout, debug):
		# Create an instance of the test unit for conversion:
		entity = unit(clk, ce, reset, dout, debug)
		name = entity.func.__name__
		design = ys.Design(name)
		# Convert into synthesis RTLIL:
		# Make sure to set `trace` when you wish to look at a VCD file:
		entity.convert("yosys_module", design, name=name, trace=True)
		return design

	design = convert(UNIT, clk, ce, reset, dout, debug)
	# Write out the verilog post-synthesis code for the cosimulation object (further below)
	design.write_verilog(design.name, True)

	return design

def standard_trace(unitname, clkname, stdtb, arst, usercfg):
	import wavedraw
	from ys_aux import setupCosimulation

	UUT = "tb_" + unitname
	
	if usercfg:
		cfg = {}
		for n, c in usercfg.items():
			cfg[UUT + n] = c
	else:
		cfg = None

	# Declare the signals:    
	clk = Signal(bool())
	ce = Signal(bool())
	dout, debug = [ Signal(intbv()[8:]) for i in range(2) ]
	reset = ResetSignal(0, 1, isasync = arst)

	@block
	def mapped_fsm(clk, ce, reset, dout, debug):
		"Wrapper for cosimulation object"
		args = locals()
		name = unitname + "_mapped"
		return setupCosimulation(name, use_assert=False, interface=args, debug=False)

	tb = stdtb(mapped_fsm, clk, ce, reset, dout, debug)
	tb.config_sim(backend = 'myhdl', timescale="1ps", trace=True)
	tb.run_sim(20)
	tb.quit_sim()

	waveform = wavedraw.vcd2wave(unitname + ".vcd", UUT + clkname, cfg)

	return waveform
		
