// External tristate buffer input example
//
//
module tristate_in
#(parameter WIDTH = 8) (
	A, Y
);

input [WIDTH-1:0] A;
output [WIDTH-1:0] Y;

assign Y = A;

endmodule
